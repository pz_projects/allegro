package tests;

import baza.ConfigurationClass;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Test;

public class HomePage_Tests extends ConfigurationClass {

    public HomePage_Tests() {
        super();
    }

    @BeforeSuite()
    public void configurations() {
        initial();
    }

    @BeforeClass
    public void setUpBeforeClass() {

    }

    @AfterSuite
    public void quit() {
        driver.quit();
    }

    @Test(priority = 1)
    public void waitForThePageTitle() {
         wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//h1[contains(text(),'Exercises for test automation of web applications')]")));
    }

}
