package tests;

import baza.ConfigurationClass;
import helpers.GlobalMethods;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.annotations.*;
import pages.ExerciseFirstPage;
import pages.HomePages;
import steps.ExerciseFirstSteps;


public class ExerciseFirst_Tests extends ConfigurationClass {

    ExerciseFirstSteps exerciseOneSteps;
    ExerciseFirstPage exerciseFirstPage;
    GlobalMethods globalMethods;

    public ExerciseFirst_Tests() {
        super();
    }

    @BeforeClass
    public void setUpBeforeClass() {
        PageFactory.initElements(driver, this);
        exerciseOneSteps = new ExerciseFirstSteps();
        exerciseFirstPage = new ExerciseFirstPage();
        globalMethods = new GlobalMethods();
    }

    @AfterMethod
    public void setUpTest(){
//      driver.navigate().back();
        driver.get(addressUrl);
    }

    @Test(priority = 1)
    public void Exercise1() {
        exerciseOneSteps.pressTheKeyAsInstructed();
        Assert.assertTrue(globalMethods.waitUntilElementVisibilityAndPrintlnNameElement(exerciseFirstPage.fieldSolution));
    }



}
