package tests;

import baza.ConfigurationClass;
import helpers.GlobalMethods;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import pages.ExerciseFirstPage;
import steps.ExerciseThirdSteps;

public class ExerciseThird_Tests extends ConfigurationClass {

    ExerciseFirstPage exerciseFirstPage;
    GlobalMethods globalMethods;
    ExerciseThirdSteps exerciseThirdSteps;


    public ExerciseThird_Tests() {
        super();
    }

    @BeforeClass
    public void setUpBeforeClass() {
        PageFactory.initElements(driver, this);
        exerciseFirstPage = new ExerciseFirstPage();
        globalMethods = new GlobalMethods();
        exerciseThirdSteps = new ExerciseThirdSteps();
    }

    @AfterMethod
    public void setUpTest(){
//      driver.navigate().back();
        driver.get(addressUrl);
    }

    @Test(priority = 1)
    public void Exercise3() {
        exerciseThirdSteps.downloadColorAndEnterFromTheSelector();
        Assert.assertTrue(globalMethods.waitUntilElementVisibilityAndPrintlnNameElement(exerciseFirstPage.fieldSolution));
    }

}
