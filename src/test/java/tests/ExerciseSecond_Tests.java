package tests;

import baza.ConfigurationClass;
import helpers.GlobalMethods;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.annotations.*;
import pages.ExerciseFirstPage;
import steps.ExerciseSecondSteps;

public class ExerciseSecond_Tests extends ConfigurationClass {

    ExerciseFirstPage exerciseFirstPage;
    ExerciseSecondSteps exerciseSecondSteps;
    GlobalMethods globalMethods;

    public ExerciseSecond_Tests() {
        super();
    }

    @BeforeClass
    public void setUpBeforeClass() {
        PageFactory.initElements(driver, this);
        exerciseFirstPage = new ExerciseFirstPage();
        exerciseSecondSteps = new ExerciseSecondSteps();
        globalMethods = new GlobalMethods();
    }

    @AfterMethod
    public void setUpTest() {
//      driver.navigate().back();
        driver.get(addressUrl);
    }

    @Test(priority = 1)
    public void Exercise2() {
        exerciseSecondSteps.downloadAndEnterText();
        Assert.assertTrue(globalMethods.waitUntilElementVisibilityAndPrintlnNameElement(exerciseFirstPage.fieldSolution));
    }

}
