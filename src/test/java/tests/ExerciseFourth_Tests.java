package tests;

import baza.ConfigurationClass;
import helpers.GlobalMethods;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import pages.ExerciseFirstPage;
import pages.HomePages;
import steps.ExerciseFourthSteps;

public class ExerciseFourth_Tests extends ConfigurationClass {

    ExerciseFirstPage exerciseFirstPage;
    GlobalMethods globalMethods;
    ExerciseFourthSteps exerciseFourthSteps;


    public ExerciseFourth_Tests() {
        super();
    }

    @BeforeClass
    public void setUpBeforeClass() {
        PageFactory.initElements(driver, this);
        exerciseFirstPage = new ExerciseFirstPage();
        globalMethods = new GlobalMethods();
        exerciseFourthSteps = new ExerciseFourthSteps();
    }

    @AfterMethod
    public void setUpTest(){
//      driver.navigate().back();
        driver.get(addressUrl);
    }

    @Test(priority = 1)
    public void Exercise4() {
        exerciseFourthSteps.markCorrectAnswer();
        Assert.assertTrue(globalMethods.waitUntilElementVisibilityAndPrintlnNameElement(exerciseFirstPage.fieldSolution));
    }
}
