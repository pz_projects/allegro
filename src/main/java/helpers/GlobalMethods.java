package helpers;

import baza.ConfigurationClass;
import org.openqa.selenium.*;
import org.testng.Assert;

import static org.openqa.selenium.support.ui.ExpectedConditions.visibilityOf;

public class GlobalMethods extends ConfigurationClass {

    public static void log(String text) {
        System.out.println(text);
    }


    public boolean waitUntilElementVisibilityAndPrintlnNameElement(WebElement element) {
        boolean result = true;
        try {
            wait.until(visibilityOf(element)).getText();
            log("Solution is correct.");
        } catch (TimeoutException | NoSuchElementException e) {
            log("Solution is not correct.");
            result = false;
        }
        return result;
    }

    public boolean isCheckboxUnchecked(WebElement checkbox, String checkboxDescription) {
        boolean statusCheckbox = checkbox.getAttribute("class").contains("active");
        if (!statusCheckbox) {
            log("Checkbox " + checkboxDescription + " is unchecked.");
            return statusCheckbox;
        } else {
            log("Checkbox " + checkboxDescription + " is checked.");
            Assert.fail("Checkbox " + checkboxDescription + " is checked.");
            return statusCheckbox;
        }
    }

    public void scrollPage(String direction) {
        scrollPage(direction, 0);
    }

    public void scrollPage(String direction, int px) {
        switch (direction) {
            case "TOP":
                ((JavascriptExecutor) driver).executeScript("window.scrollTo(0,0);");
                break;
            case "BOTTOM":
                ((JavascriptExecutor) driver).executeScript("window.scrollTo(0,document.body.scrollHeight);");
                break;
            case "UP":
                ((JavascriptExecutor) driver).executeScript("window.scrollTo(0," + px + ");");
                break;
            case "DOWN":
                ((JavascriptExecutor) driver).executeScript("window.scrollTo(0," + -px + ");");
                break;
            case "RIGHT":
                ((JavascriptExecutor) driver).executeScript("window.scrollTo(" + px + ",0);");
                break;
            case "LEFT":
                ((JavascriptExecutor) driver).executeScript("window.scrollTo(" + -px + ",0);");
                break;
            default:
                break;
        }
    }


}
