package steps;

import helpers.GlobalMethods;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;
import pages.ExerciseFirstPage;
import pages.ExerciseThirdPage;
import pages.HomePages;

import static org.openqa.selenium.support.ui.ExpectedConditions.visibilityOf;

public class ExerciseThirdSteps extends ExerciseThirdPage {

    ExerciseFirstPage exerciseFirstPage;
    HomePages homePages;

    public ExerciseThirdSteps() {
        PageFactory.initElements(driver, this);
        exerciseFirstPage = new ExerciseFirstPage();
        homePages = new HomePages();
    }

    public void selectMode(WebElement element, String selectText) {
        wait.until(visibilityOf(element));
        Select select = new Select(element);
        select.selectByVisibleText(selectText);
    }

    private String waitElementToVisibilityOfAndDownloadText(WebElement element) {
        return wait.until(visibilityOf(element)).getText();
    }

    public void downloadColorAndEnterFromTheSelector() {
        homePages.buttonExerciseThird.click();
        String downloadColor = waitElementToVisibilityOfAndDownloadText(fieldDownloadText);
        selectMode(selectColors, downloadColor);
//      exerciseFirstPage.buttonCheckSolution.click();
        js.executeScript("arguments[0].click();", exerciseFirstPage.buttonCheckSolution);
    }


}

