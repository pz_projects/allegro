package steps;

import helpers.GlobalMethods;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import pages.ExerciseFirstPage;
import pages.HomePages;

import java.util.List;

public class ExerciseFirstSteps extends ExerciseFirstPage {

    ExerciseFirstPage exerciseFirstPage;
    HomePages homePages;

    public ExerciseFirstSteps() {
        PageFactory.initElements(driver, this);
        exerciseFirstPage = new ExerciseFirstPage();
        homePages = new HomePages();
    }

    public void pressTheKeyAsInstructed() {
        homePages.buttonExerciseFirst.click();
        List<WebElement> buttons = driver.findElements(exerciseFirstPage.keyToBePressed);
        for (WebElement button : buttons) {
            if (button.getText().equalsIgnoreCase("B1")) {
                buttonB1.click();
            } else {
                buttonB2.click();
            }
        }
        buttonCheckSolution.click();
    }


}

