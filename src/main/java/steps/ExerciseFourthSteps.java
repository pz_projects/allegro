package steps;

import baza.ConfigurationClass;
import helpers.GlobalMethods;
import org.openqa.selenium.By;
import org.openqa.selenium.support.PageFactory;
import pages.ExerciseFirstPage;
import pages.HomePages;


public class ExerciseFourthSteps extends ConfigurationClass {

    ExerciseFirstPage exerciseFirstPage;
    HomePages homePages;

    public ExerciseFourthSteps() {
        PageFactory.initElements(driver, this);
        exerciseFirstPage = new ExerciseFirstPage();
        homePages = new HomePages();
    }

    public void markCorrectAnswer() {
        homePages.buttonExerciseFourth.click();
        scrollPage(1);
        scrollPage(2);
        scrollPage(3);
        scrollPage(4);
        exerciseFirstPage.buttonCheckSolution.click();
    }

    public void scrollPage(int group) {
        int numberOfDownloadedAnswer = 1 + group;
        int numberOFMarkedAnswer = 0 + group;
        switch (driver.findElement(By.xpath("//tr[" + numberOfDownloadedAnswer + "]/td[2]/code")).getText()) {
            case "Beluga Brown":
                driver.findElement(By.xpath("//div[1]/div[" + numberOFMarkedAnswer + "]/input[1]")).click();
                break;
            case "Mango Orange":
                driver.findElement(By.xpath("//div[1]/div[" + numberOFMarkedAnswer + "]/input[2]")).click();
                break;
            case "Verdoro Green":
                driver.findElement(By.xpath("//div[1]/div[" + numberOFMarkedAnswer + "]/input[3]")).click();
                break;
            case "Freudian Gilt":
                driver.findElement(By.xpath("//div[1]/div[" + numberOFMarkedAnswer + "]/input[4]")).click();
                break;
            case "Pink Kong":
                driver.findElement(By.xpath("//div[1]/div[" + numberOFMarkedAnswer + "]/input[5]")).click();
                break;
            case "Duck Egg Blue":
                driver.findElement(By.xpath("//div[1]/div[" + numberOFMarkedAnswer + "]/input[6]")).click();
                break;
            case "Anti - Establishment Mint":
                driver.findElement(By.xpath("//div[1]/div[" + numberOFMarkedAnswer + "]/input[7]")).click();
                break;
            case "Amberlite Firemist":
                driver.findElement(By.xpath("//div[1]/div[" + numberOFMarkedAnswer + "]/input[8]")).click();
                break;
            default:
                break;
        }
    }

}

