package steps;

import helpers.GlobalMethods;
import org.openqa.selenium.support.PageFactory;
import pages.ExerciseFirstPage;
import pages.ExerciseSecondPage;
import pages.HomePages;

public class ExerciseSecondSteps extends ExerciseSecondPage {

    HomePages homePages;
    ExerciseFirstPage exerciseFirstPage;

    public ExerciseSecondSteps() {
        PageFactory.initElements(driver, this);
        homePages = new HomePages();
        exerciseFirstPage = new ExerciseFirstPage();
    }

    public void downloadAndEnterText() {
        homePages.buttonExerciseSecond.click();
        String downloadText = fieldDownloadText.getText();
        fieldEnterText.clear();
        fieldEnterText.sendKeys(downloadText);
        exerciseFirstPage.buttonB1.click();
        exerciseFirstPage.buttonCheckSolution.click();
    }

}

