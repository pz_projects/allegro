package baza;

import org.openqa.selenium.Dimension;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

public class ConfigurationClass {

    public static WebDriver driver;
    public static WebDriverWait wait;
    public static JavascriptExecutor js;
    public static String addressUrl;
    public static String userName;

    public static void initial() {

        addressUrl = System.getProperty("Adres_Srodowiska");

        addressUrl = "https://antycaptcha.amberteam.pl/";

        userName = new com.sun.security.auth.module.NTSystem().getName();
        System.out.println("\n");
        System.out.println("******************************************");
        System.out.println("Currently logged in user (starting the process): " + userName);
        System.out.println("******************************************\n");

        System.setProperty("webdriver.chrome.driver", "src/main/resources/testSiute/chromedriver.exe");
        driver = new ChromeDriver();
        driver.get(addressUrl);

        Dimension dimension = new Dimension(1920, 1080);
        wait = new WebDriverWait(driver, 20);
        driver.manage().window().setSize(dimension);

        js = (JavascriptExecutor) driver;
    }

}
