package pages;

import baza.ConfigurationClass;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class ExerciseSecondPage extends ConfigurationClass {

    public ExerciseSecondPage() {
        PageFactory.initElements(driver, this);
    }

    @FindBy(xpath = "//code[contains(text(),'T14')]//preceding::code")
    public WebElement fieldDownloadText;

    @FindBy(id = "t14")
    public WebElement fieldEnterText;


}
