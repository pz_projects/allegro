package pages;

import baza.ConfigurationClass;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class HomePages extends ConfigurationClass {

    public HomePages(){
        PageFactory.initElements(driver, this);
    }

    @FindBy(xpath = "//h1[contains(text(),'Exercises for test automation of web applications')]")
    public WebElement logoHome;

    @FindBy(xpath = "//*[contains(text(),'1 - Three buttons')]")
    public WebElement buttonExerciseFirst;

    @FindBy(xpath = "//a[contains(text(),'2 - Editbox')]")
    public WebElement buttonExerciseSecond;

    @FindBy(xpath = "//a[contains(text(),'3 - Dropdown list')]")
    public WebElement buttonExerciseThird;

    @FindBy(xpath = "//a[contains(text(),'4 - Radio buttons')]")
    public WebElement buttonExerciseFourth;

}
