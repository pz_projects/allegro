package pages;

import baza.ConfigurationClass;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;


public class ExerciseFirstPage extends ConfigurationClass {

    public ExerciseFirstPage() {
        PageFactory.initElements(driver, this);
    }

    @FindBy(id = "btnButton1")
    public WebElement buttonB1;

    @FindBy(xpath = "//*[@class='six columns']//*[contains(text(),'B2')]")
    public WebElement buttonB2;

//    @FindBy(xpath = "//tr[*]/td[2]//*")
//    public List<WebElement> keyToBePressed;

    public By keyToBePressed = By.xpath("//tr[*]/td[2]//*");

    @FindBy(id = "solution")
    public WebElement buttonCheckSolution;

    @FindBy(xpath = "//code[contains(text(),'OK. Good answer')]")
    public WebElement fieldSolution;

}
