package pages;

import baza.ConfigurationClass;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class ExerciseThirdPage extends ConfigurationClass {

    public ExerciseThirdPage() {
        PageFactory.initElements(driver, this);
    }

    @FindBy(xpath = "//tr[2]/td[2]/code")
    public WebElement fieldDownloadText;

    @FindBy(id = "s13")
    public WebElement selectColors;

}
